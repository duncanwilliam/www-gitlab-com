---
layout: handbook-page-toc
title: "GitLab Inc (US) Benefits"
description: "GitLab Inc (US) benefits specific to US based team members."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

----

* Navigate to [Total Rewards Processes & Audits for US Benefits](/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/total-rewards-processes/)

## Specific to US based team members
 
US based benefits are arranged through Willis Towers Watson and managed on the [PlanSource platform](https://benefits.plansource.com.). The benefits decision discussions are held by Total Rewards, the CFO, and the CEO to elect the next year's benefits by the carrier's deadlines. Total Rewards will notify the team of open enrollment as soon as details become available. 
 
**The 2021 Open Enrollment for the 2022 plan year will take place November 8th through November 19th. Your plan will renew January 1, 2022. This will be a passive enrollment, it is not required to log into PlanSource to complete your enrollment unless you wish to make a change _and/or to enroll in a Flexible Spending Account (FSA) or Dependent Care FSA (DCFSA) for 2022. You will not have an FSA or DCFSA in 2022 if you do not elect into them during Open Enrollment._** Information and a digital benefits guide for 2022 will be made available soon
 
Please review the full [summary plan description](https://drive.google.com/file/d/1SAmlBXC6AZUy3vSNRjdcEgQIFCQP01L6/view?usp=sharing) of all related health benefits.
 
## Who To Contact
 
* For benefit inquiries: WTW Benefits Helpline `877-409-9288` or `GitLab@willistowerswatson.com` 
* For support with the PlanSource system: `total-rewards@ gitlab`
* For questions regarding internal policies, 401k, approval of Qualifying Life Events: `total-rewards@ gitlab`
 
## ID Cards
 
Physical carrier ID cards for medical and vision are normally received within 3 weeks of submitting your benefit elections. If you or your medical providers are in need of immediate confirmation of your coverage, please contact the carrier directly. Please note there are no physical ID cards for dental coverage.

### Digital ID Cards
 
Please allow up to two weeks for ID cards to generate after completing your enrollment.
 
 * *Cigna:* Digital ID cards are available through the [myCigna](https://my.cigna.com/web/public/guest) website or the [myCigna mobile app](https://www.cigna.com/individuals-families/member-resources/mobile-apps/). 
* *Kaiser:* Digital ID cards are available through the [Kaiser website](https://healthy.kaiserpermanente.org/front-door) or the [Kaiser mobile app](https://healthy.kaiserpermanente.org/pages/mobile-app).
 
## Enrolling in Benefits
 
If you have existing coverage when joining GitLab (e.g. coverage for an additional month from your prior employer), you have the option of enrolling in GitLab's coverage after your prior coverage terminates. If you wish to do this, you should register with PlanSource during onboarding and waive all coverages. Once your previous coverage is terminated, you should sign up for coverage through PlanSource on or within 30 days of the termination date by initiating a Qualifying Life Event and providing proof of coverage termination.
 
GitLab covers **100% of team member contributions and 66% for spouse, dependents, and/or domestic partner** of premiums for medical, dental, and vision coverage. Plan rates are locked through December 31, 2020.
 
More information on the processed deductions in payroll from PlanSource can be found on the [Accounting and Reporting page](/handbook/finance/accounting/).

For questions on benefits, please see the following [PlanSource Video Library](https://videolibrary.plansource.com/). 

### Logging into PlanSource

You can log into PlanSource [here](https://benefits.plansource.com) with the following information:
* Username:  Your username is the first letter of your first name, the first 6 letters of your last name, and the last 4 digits of your social security number. 
  * Example: John Hancock (SS# XXX-XX-6789) would have the following user name: jhancoc6789
* Password:  Your password is your birth date in YYYYMMDD format. 
  * Example:  John Hancock birth date is January 7, 1968 would have the following password: 19680107

Once you have logged in, you will be prompted to update your password.

### Group Medical Coverage
 
GitLab offers plans from Cigna for all states within the US as well as additional Kaiser options for residents of California, Hawaii, and Colorado. Deductibles for a plan year of 2021-01-01 to 2021-12-31.
 
_If you already have current group medical coverage, you may choose to waive or opt out of group health benefits. If you choose to waive health coverage, you will receive a $300.00 monthly benefit allowance and will still be able to enroll in dental, vision, optional plans, and flexible spending accounts._
 
If you do not enroll in a plan within your benefits election period, you will automatically receive the medical waiver allowance.
 
GitLab has confirmed that our medical plans are CREDITABLE. Please see the attached [2021 notice](https://drive.google.com/file/d/1_w2svtOAP09lVKLWXsKzyLhZdlmB2ElZ/view?usp=sharing). If you or your dependents are Medicare eligible or are approaching Medicare eligibility, you will need this notice to confirm your status when enrolling for Medicare Part D. For more information about Medicare prescription drug coverage:
- Visit www.medicare.gov
- Call your State Health Insurance Assistance Program (see the [“Medicare & You” handbook](https://www.medicare.gov/medicare-and-you) for their telephone number) for personalized help.
- Call 1-800-MEDICARE (1-800-633-4227). TTY users should call 1-877-486-2048.
 
#### Eligibility
 
Any active, regular, full-time team member working a minimum of 30 hours per week are eligible for all benefits. Benefits are effective on your date of hire. Others eligible for benefits include:
 * Your legal spouse or domestic partner,
 * Your dependent children up until age 26 (including legally adopted and stepchildren), and/or
 * Any dependent child who reaches the limiting age and is incapable of self-support because of a mental or physical disability
 
Note: If you and an eligible dependent (as defined above) are both employed by GitLab, you may only be covered by GitLab’s coverage once. This also applies to enrolling in either your own supplemental life insurance or supplemental spouse/dependent life insurance through your dependent who is employed by GitLab, but not both.
 
#### Qualifying Life Events
 
Due to IRS guidelines, you cannot make changes to your health insurance benefits outside of GitLab's annual open enrollment period unless you experience a [Qualifying Life Event](https://www.healthcare.gov/glossary/qualifying-life-event/).
 
A QLE is a change in your situation — like getting married, having a baby, etc that can make you eligible for a special enrollment period. You have ***30 days from the date of your qualifying event*** to submit your requested change to PlanSource. 
 
##### Process for submitting a QLE

1. Log into [PlanSource](https://benefits.plansource.com/).
1. Select `Update My Benefits`.
1. Select the Life Event that applies to your situation from the list.
1. Follow the prompts to review your information and make any changes to dependent(s), if applicable.
1. Customize your benefits package with any changes. Please note that the system will require you to go into each coverage, even if you had previously declined and would like to continue to decline or are similarly making no changes to the coverage. 
1. Once you have made all  of your changes, please click the button to Review & Check Out.
1. You will be taken to a screen to review the costs of your benefits and will need to click Checkout again to confirm your changes.

TODO Add QLE documentation info
 
#### Cigna Medical Plans (2021)
 
Effective January 1, 2021, GitLab will be transitioning from UHC to Cigna as the primary national carrier.
 
##### Cigna 2021 & 2022 Calendar Year Plans
 
**Coverages:**
 
 The Cigna 2022 plan details will remain the same as the 2021 plan details listed below, but will include the following enhancements: inclusion of acupuncture, chiropractic care, and hearing aid benefits.
 
In Network:
 
| Plan Details               | [2021 Cigna HDHP- HSA ](https://drive.google.com/file/d/1snzJv2JsItipBS12ueibzedjZMgLjs_e/view?usp=sharing) <BR> 2022 Cigna HDHP- HSA (TBD) | [2021 Cigna EPO (PPO $0)**](https://drive.google.com/file/d/147z46SlhXLGUhoXVjQTxI7Qs5ROL6ZLr/view?usp=sharing)<BR> 2022 Cigna EPO (PPO $0)** (TBD)  | [2021 Cigna PPO 500***](https://drive.google.com/file/d/1utEEBXzbvIY4guFt0asM86waDWVMdLHQ/view?usp=sharing)  <BR> 2022 Cigna PPO 500*** (TBD)    |
|:---------------------------|:----------------------:|:------------------:|:-----------------:|
| Deductible (Single/Family) | $2,000 / $2,800        | $0 / $0            | $500 / $1,000     |
| Out of Pocket Max (Single/Family)    | $4,000 / $8,000        | $2,500 / $5,000    | $3,000 / $6,000   |
| Primary Care Visit         | 20%                    | $20 per visit      | $20 per visit     |
| Specialist Visit           | 20%                    | $20 per visit      | $20 per visit     |
| Urgent Care                | 20%                    | $50 per visit      | $50 per visit     |
| Emergency Room             | 20%                    | $100 per visit     | $100 per visit    |
| Hospital Inpatient         | 20%                    | $250 per admission | 10%               |
| Hospital Outpatient        | 20%                    | 0%                 | 10%               |
| Generic                    | $10                    | $10                | $10               |
| Brand - Preferred          | $30                    | $30                | $30               |
| Brand - Non-Preferred      | $50                    | $50                | $50               |
| Rx Plan^                   | [Standard 3-tier](https://www.cigna.com/static/www-cigna-com/docs/individuals-families/member-resources/prescription/standard-3-tier.pdf) | [Standard 3-tier](https://www.cigna.com/static/www-cigna-com/docs/individuals-families/member-resources/prescription/standard-3-tier.pdf) | [Standard 3-tier](https://www.cigna.com/static/www-cigna-com/docs/individuals-families/member-resources/prescription/standard-3-tier.pdf) |
 
** In order for the EPO plan to be compliant in all states, it has been set up as a PPO plan with bad out-of-networks benefits including a deductible of $10k/$20k, an Out of Pocket Max of $20k/$40k, and a coinsurance of 50%. Please do not enroll in this plan if you are intending to use the out-of-networks benefits and instead review the Cigna PPO 500 or Cigna HSA plans.
 
*** Cigna will provide an extended network PPO Plan for Utah team members which will include the Intermountain Healthcare System.
 
^ The linked Prescription Drug List is subject to change. When making a change mid-year, Cigna states that they will send out the following communication to impacted members: centralized notification 60 days prior, 2 letters before the change, and 1 letter after the change.
 
**Team Member Costs:**
 
_2021 Rates_: The following costs are monthly rates which would be deducted from your paycheck.
 
| Tier                     | HDHP-HSA | EPO  | PPO  |
|--------------------------|:---:|:----:|:----:|
| Team Member Only         | $0  | $0   | $0   |
| Team Member + Spouse     | $0  | $234 | $222 |
| Team Member + Child(ren) | $0  | $180 | $168 |             
| Family                   | $0  | $402 | $378 |

_2022 Rates_: The following costs are monthly rates which would be deducted from your paycheck.
 
| Tier                     | HDHP-HSA | EPO  | PPO  |
|--------------------------|:---:|:----:|:----:|
| Team Member Only         | $0  | $0   | $0   |
| Team Member + Spouse     | $0  | $248 | $242 |
| Team Member + Child(ren) | $0  | $188 | $184 |             
| Family                   | $0  | $426 | $416 |

Note: For the **team member only HSA**, GitLab will contribute $100 per month. For residents of California, Alabama, and New Jersey this additional contribution is taxable on the state level. Effective January 1, 2022, GitLab will expand the $100 per month HSA employer contribution to all Tiers. 

##### Cigna Infertility Services
 
Infertility services will be included in both the PPO 500 plan and HSA plan. This includes a $15,000 lifetime infertility benefit for each covered member. More details will be included in the SBCs once available.
 
##### Cigna Transgender Benefit Coverage
 
Cigna has advised they will provide [WPATH](https://www.wpath.org/) compliant coverage for all plans. For more information on their coverage, please see the [coverage policy document](https://drive.google.com/file/d/1sdsiFcTFEWsIidOXwPBcNNOCqFVERSXg/view?usp=sharing). For Cigna to provide coverage, medical necessity must be proven. It is highly recommended to go through the prior authorization process when seeking treatment.
 
Please reach out to the [Total Rewards team and WTW](/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/#who-to-contact) with any questions or if you need help while seeking authorization for a treatment.
 
##### Cigna Provider Search
 
1. Visit [myCigna](https://my.cigna.com/web/public/guest). If you haven't previously registered for an account, you will want to register.
1. Select "Find Care & Costs" at the top of the page.
1. Here you can select to search by Doctor Type, Name, Reason for Visit, Facility, or you can price a medication at local pharmacies.
1. When you scroll down, there are quick links for your nearest urgent care and telehealth.
 
IMPORTANT NOTE:
 
As an additional measure to confirm “Contracted Providers” (i.e. In-Network), it’s always a good idea to follow up with Providers directly (phone, email or otherwise) to additionally confirm if a Provider is contracted with Cigna or not.
Provider contracts change and sometimes the contracts change faster than the website maintenance teams for the insurance carriers. It's better to spend time researching and confirming rather than assuming and being stuck with *balance billing* from Out of Network providers.
*Balance billing* is if the Out of Network provider's charge is $100 and the carrier’s maximum allowed reimbursement (MAR) amount is $70, the provider is within rights to bill you for the remaining $30. A “Cigna Contracted Provider” cannot balance bill Cigna members.
 
##### Cigna Telehealth
 
Virtual visits for Cigna members can be accessed by visiting [myCigna](https://my.cigna.com/web/public/guest), selecting "Find Care & Costs" and scrolling down to the bottom of the page.
 
#### Kaiser Medical Plans
 
##### Kaiser 2021 & 2022 Calendar Year Plans
 
In addition to the Cigna plans offered above, the following plans are also an option for team members in the United States located in California, Colorado, and Hawaii.
 
**Coverages:**

 The Kaiser 2022 plan details will remain the same as the 2021 plan details listed below, but will include the following enhancements: inclusion of acupuncture, chiropractic care, and hearing aid benefits. 
 
| Plan Details               | [2021 HMO 20 NorCal](https://drive.google.com/file/d/1kgJGxaVfAwdN5E2fD0gbzKFWW1eCyU56/view?usp=sharing)  <br> 2022 HMO NorCal (TBD)     | [2021 HMO 20 SoCal](https://drive.google.com/file/d/1UtnmvtKmn4PKETOphAQ7e54lYu32NGb_/view?usp=sharing)  <br> 2022 HMO 20 SoCal (TBD) | [2021 HMO 20 CO](https://drive.google.com/file/d/1MuG9MQumOq3OjAMtwjil33RKkWdNrAa7/view?usp=sharing) <BR>  [2022 HMO 20 CO](https://drive.google.com/file/d/1BosRYdWVsrdpLpaGP--ci79zmTbFPOto/view?usp=sharing)    | [2021 HMO 20 HI](https://drive.google.com/file/d/1oLiQJkVcb2YLuvB-adEJPwf4knM1R9J3/view?usp=sharing)   <BR>  2022 HMO 20 HI (TBD)   |
|----------------------------|:--------------------:|:---------------:|:---------------:|:---------------:|
| Deductible (Single/Family) | $0 / $0              | $0 / $0         | $0 / $0         | $0 / $0         |
| Out Of Pocket Max (Single/Family)    | $1,500 / $3,000      | $1,500 / $3,000 | $2,000 / $4,000 | $2,500 / $7,500 |
| PCP/Specialist Copay       | $20 / $35            | $20 / $35       | $20 / $35       | $15 / $15       |
| Emergency Room             | $100                 | $100            | $250            | $100            |
| Urgent Care                | $20                  | $20             | $50             | $15             |
| Hospital Inpatient         | $250/admit           | $250/admit      | $300/admit      | 10%             |
| Hospital Outpatient        | $35/procedure        | $35/procedure   | $100/procedure  | 10%             |
| **Rx - Deductible**        |                      |                 |                 |                 |
| Generic                    | $10                  | $10             | $10             | See SBC (tier 1), $3 (tier 1a), $15 (tier 1b)             |
| Brand - Preferred          | $35                  | $35             | $30             | $50             |
| Brand - Non-Preferred      | $35                  | $35             | $50             | $50             |
| Specialty Drugs            | 20% up to $150       | 20% up to $150  | 20% up to $250  | $200            |
 
**Team Member Costs:**
 
The following costs are monthly rates which would be deducted from your paycheck.

_2021 Rates_: The following costs are monthly rates which would be deducted from your paycheck.
 
| Tier                     | HMO CA North | HMO CA South | HMO CO | HMO HI |
|--------------------------|:------------:|:------------:|:------:|:------:|
| Team Member Only         | $0           | $0           | $0     | $0     |
| Team Member + Spouse     | $240         | $240         | $276   | $174   |
| Team Member + Child(ren) | $192         | $192         | $228   | $138   |         
| Family                   | $366         | $366         | $456   | $342   |


_2022 Rates_: The following costs are monthly rates which would be deducted from your paycheck.
 
| Tier                     | HMO CA North | HMO CA South | HMO CO | HMO HI |
|--------------------------|:------------:|:------------:|:------:|:------:|
| Team Member Only         | $0           | $0           | $0     | $0     |
| Team Member + Spouse     | $244         | $244         | $340   | $178   |
| Team Member + Child(ren) | $194         | $194         | $284   | $144   |         
| Family                   | $378         | $378         | $566   | $356   |
 
##### Kaiser Telehealth
 
Virtual visits for Kaiser members can be accessed by logging into Kaiser's [online portal](https://healthy.kaiserpermanente.org/). Please consult the online portal and your plan details for your copay amount.
 
##### Kaiser Period to Submit Claims
 
For in-network services: N/A.
 
For out-of-network services: 365 days from Date of Service.
 
#### Pregnancy & Maternity Care
 
With medical plans, GitLab offers pregnancy and maternity care. Depending on the plan you selected, your coverages may differ for in-network vs out-of-network, visits, and inpatient care. Please contact the [Total Rewards team or WTW](/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/#who-to-contact) with any questions about your plan. Once your child has arrived, please follow the steps outlined above in regard to this [Qualifying Life Event](/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/#qualifying-life-events).
 
### Dental
 
Dental is provided by Cigna, plan: DPPO.
 
Dental does not come with individualized insurance cards from Cigna, although you can download them by setting up a Cigna account through the [Cigna website](https://my.cigna.com). [Cigna's site and app]([Total Rewards team and WTW](/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/#id-cards) will house individualized ID cards team members can access at any time. For the most part, dental providers do not request or require ID cards as they look up insurance through your social security number. If you need additional information for a claim please let the [Total Rewards team or WTW](/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/#who-to-contact) know. Cigna'a mailing address is PO Box 188037 Chattanooga, TN, 37422 and the direct phone number is 800-244-6224.
 
When submitting a claim, you can mail it to Cigna Dental PO Box 188037 Chattanooga, TN, 37422 or fax it to 859-550-2662.
 
#### Dental 2021 & 2022 Calendar Year Plan
 
**Coverages:**

 The 2022 Dental plan details will remain the same as the 2021 plan details listed below.

 
| Plan Details                         | [DPPO](https://drive.google.com/file/d/1s8x3BJ36hJ5gU-vn29dQSOFVkhvm1rH4/view?usp=sharing)       | 
|--------------------------------------|:----------:|
| Deductible (Single/Family)                          | $50 / $150 |
| Maximum Benefit                      | $2,000     |
| Preventive Care CoInsurance (in/out) | 0% / 0%    |
| Basic Care Coinsurance (in/out)      | 20% / 20%  |
| Major Care Coinsurance (in/out)      | 50% / 50%  |
| Out of Network Reimbursement         | 90th R&C   |
| **Orthodontia**                      |            |
| Orthodontic Coinsurance (in/out)     | 50% / 50%  |
| Orthodontic Max Benefits             | $1,500     |
 
**Team Member Costs:**
 
The following costs are monthly rates which would be deducted from your paycheck.

 _2021 Rates_: The following costs are monthly rates which would be deducted from your paycheck.

| Tier                     | DPPO |
|--------------------------|:----:|
| Team Member Only         | $0   |
| Team Member + Spouse     | $12  |
| Team Member + Child(ren) | $18  |
| Family                   | $36  |

 _2022 Rates_: The following costs are monthly rates which would be deducted from your paycheck.

| Tier                     | DPPO |
|--------------------------|:----:|
| Team Member Only         | $0   |
| Team Member + Spouse     | $14  |
| Team Member + Child(ren) | $20  |
| Family                   | $36  |
 
##### Cigna Dental Period to Submit Claims
 
For in-network services: N/A.
 
For out-of-network services: 365 days from Date of Service.
 
### Vision
 
Vision is provided by Cigna.
 
When submitting a claim, you can mail it to Cigna Vision PO Box 385018 Birmingham, AL 35238 or submit it online using the following instructions:
 
1. Log in or register an account at https://cigna.vsp.com/.
1. Navigate to "Claims & Reimbursement" on the left panel.
1. Choose yourself or dependent from the dropdown depending who the claim is for.
1. Expand the "Customer Reimbursement Form" section.
1. Click "Continue" to be taken to the online claim form. Make sure you attach an itemized receipt when prompted.
 
#### Vision 2021 & 2022 Calendar Year Plan
 
**Coverages:**

 The 2022 Vision plan details will remain the same as the 2021 plan details listed below, but will include the following enhancements: increase frame allowance to up to $150. 

 
| Plan Details                      | [Vision](https://drive.google.com/file/d/1iqeSHND7WzTouvP-Rzc6kw8THxz2DlDL/view?usp=sharing)       | 
|-----------------------------------|:------------:|
| Frequency of Services             | 12 months   |
| Copay Exam                        | $20          | 
| Copay Materials                   | -            |
| Single Vision                     | $0           | 
| Bifocal                           | $0           |
| Trifocal                          | $0           |
| Frame Allowance                   | up to $130   |
| Elective Lenses Contact Allowance | up to $130   |
 
**The Frame allowance has increase to up to $150 for the Vision 2022 Calendar Year Plan.**

**Team Member Costs:**
 
The following costs are monthly rates which would be deducted from your paycheck.

 _2021 & 2022 Rates_: The following costs are monthly rates which would be deducted from your paycheck.
 
| Tier                     | Vision |
|--------------------------|:------:|
| Team Member Only         | $0     |
| Team Member + Spouse     | $2.40  |
| Team Member + Child(ren) | $1.80  |
| Family                   | $4.80  |



##### Cigna Vision Period to Submit Claims
 
For in-network services: 365 days from Date of Service.
 
For out-of-network services: 365 days from Date of Service.
 
### Basic Life Insurance and AD&D
 
GitLab offers company paid basic life and accidental death and dismemberment (AD&D) plans through Cigna. The Company pays for basic life insurance coverage valued at two times annual base salary with a maximum benefit of $250,000, which includes an equal amount of AD&D coverage. For more information please see the [summary of benefits](https://drive.google.com/file/d/1rkA8n3zgZvnoiqzu3ZJuLwZ_9zMaNbZW/view?usp=sharing).
 
### Group Long-Term and Short-Term Disability Insurance
 
GitLab provides, at no cost to our Inc and Federal team members, a policy through Cigna that may replace up to 66.7% of your base salary, for qualifying disabilities. For short-term disability there is a weekly maximum benefit of $2,500; for long-term disability there is a monthly benefit maximum of $12,500.
 
**GitLab Process for Disability Claim**
 
1. If a team member will be unable to work due to disability as defined by the applicable short term disability plan for less than 25 calendar days, no action is needed and the absence will be categorized under [paid time off](/handbook/paid-time-off/).
1. Since the short-term disability insurance has a 7-day waiting period, the team member should decide on day 18 whether they will be able to return to work after 25 calendar days. If they will not be able to return, they should inform the Total Rewards team of their intent to go on short-term disability and apply for short-term disability at this time by sending the Total Rewards team a completed [Leave Request Form](https://drive.google.com/file/d/1guydUTEc0vBFMaa_IsSktZ5hXAbOXdvD/view?usp=sharing). While the team member is on short-term disability (which covers 66.7%), GitLab will supplement the remaining 33.3% through payroll if the team member has been employed for more than six months. Benefit coverage will also continue for the time the team member is on short-term disability.
1. At the end of the maximum benefit period for short-term disability of 12 weeks, the team member will determine whether they are able to return back to work.
   * If the team member intends to return on or before the end of the 12 weeks, they should provide the Total Rewards team a Return to Work authorization from their doctor stating that they are able to return to work and listing any accomodations needed, if applicable, before their return date. The Total Rewards team should request this a week before the team member's anticipated return to work date if not already provided by the team member.
   * If the team member is unable to return, the team member will be moved to unpaid leave and will have the option to continue their benefits by electing [COBRA coverage](https://www.dol.gov/sites/dolgov/files/ebsa/about-ebsa/our-activities/resource-center/faqs/cobra-continuation-health-coverage-consumer.pdf). The team member will be eligible to apply for long-term disability at this time.
 
**Short-Term Disability Claim Process via Cigna**
 
1. Team member will submit the [Short-Term Disability](https://drive.google.com/file/d/1guydUTEc0vBFMaa_IsSktZ5hXAbOXdvD/view?usp=sharing) form by email to the Total Rewards team at `total-rewards@gitlab.com`. 
     - Page 3 includes the Employee portion of the form as well as the Doctor Certification. This page can be sent directly to Cigna (using the mail or fax number at the top of the form) or this page can be returned to GitLab and we can send to Cigna all at once. This is completely at the preference of the team member or the requirement from the Doctor.
2. The Total Rewards team will complete the employer portion of the Short-Term Disability form and send to Cigna via email: `dallasfco.intake2@cigna.com`
3. The Total Rewards team will notify the team member of submission of claim and provided next steps.
4. Cigna Claims Process:  
     - Within 3 business days of Cigna receiving the claim, their claims team will contact the team member to gather additional medical or eligibility data, if needed. 
     - The claims team will also contact the Total Rewards team to confirm eligibility and verify job responsibilities, if needed. 
     - Cigna claims team will immediately begin reviewing the information available to make a decision. Cigna may also contact the team members attending physician, if needed, once Cigna has the team members authorization to do so. 
     - If the claim is denied, the team member will receive a call from Cigna explaining the decision. The Total Rewards team will also receive a notification of the denial.
     - If approved, communication is sent to the Total Rewards team and claim status reports with approval date and estimated return-to-work date is provided to the Total Rewards team.  
5. The Total Rewards team will process the approval or denial and file all related paperwork in BambooHR. 
 
## Employee Assistance Program
 
GitLab team members in the United States are eligible for a complementary [Employee Assistance program](https://www.cigna.com/individuals-families/member-resources/employee-assistance-program) as a result of enrollment in the long-term disability plan through Cigna, dependents who are enrolled in a Cigna coverage are also eligible. More information can be found online on [myCigna](https://my.cigna.com/web/public/guest) for the following topics: Emotional Health and Family Support, Home Life Referrals, Financial and Legal Assistance, Job and Career Support, and other topics.
 
## 401k Plan
 
The company offers a 401k plan in which you may make voluntary pre-tax contributions toward your retirement.
 
### Administrative Details of 401k Plan
 
1. You are eligible to participate in GitLab’s 401k as of your hire date. There is no auto-enrollment. You must actively elect your deductions.
1. You will receive an invitation from [Betterment](https://www.betterment.com) who is GitLab's plan fiduciary. For more information about Betterment please check out this [YouTube Video](https://www.youtube.com/watch?v=A-9II-zBq1k).
1. Any changes to your plan information will be effective on the next available payroll.
1. Once inside the platform you may elect your annual/pay-period contributions and investments.
1. If you have any questions about making changes to your elections, we recommend that you reach out to Betterment directly, by chat in the app, by phone at 855-906-5281, or by email at `support@betterment.com`. There is also a [Help section](https://www.betterment.com/resources/tags/help/) on the Betterment site.
1. Please review the [Summary Plan Document](https://drive.google.com/file/d/1k1xWqZ-2HjOWLv_oFCcyBkTjGnuv1l_B/view?usp=sharing) and [QDIA & Fee Disclosure](https://drive.google.com/file/d/10-nOQTAsYqj1S6xrRFzTa4-H4qQnOOa3/view?usp=sharing). If you have any questions about the plan or the documents, please reach out to Total Rewards at `total-rewards@domain`. Total Rewards is not able to advise you on your financial decisions.
1. ADP payroll system monitors and will stop the 401(k) contribution when you have reached the IRS limit for the year. Please keep in mind, if you have prior contributions from another employer, ADP will not have access to this information.
1. If your employment with GitLab terminates and you are unable to access your Betterment account due to this being connected to your GitLab email, please contact Betterment at 646-600-8263 to have the email address on file updated.
 
### 401(k) Match
 
GitLab offers matching 50% of contributions on the first 6% of allocated earnings with a yearly cap of 1,500 USD. As you are eligible to participate in GitLab's 401k as of your hire date, you are *also* eligible for GitLab matching contributions as of your hire date.
 
All employer contributions are pre-tax contributions. Team members can still make Roth 401(k) team member contributions and receive pre-tax employer contributions to your Traditional 401(k) account.
 
**Vesting:**
 
Employer contributions vest according to the following schedule:
 
| Years of Vesting Service             | Vesting Percentage |
|--------------------------------------|:------------------:|
| Less than One Year                   | 0%                 |
| One Year but less than Two Years     | 25%                |
| Two Years but less than Three Years  | 50%                |
| Three Years but less than Four Years | 75%                |
| Four or More Years                   | 100%               |
 
*Team Member* contributions are the assets of those team members and are not applicable to this vesting schedule.
 
**Vesting example**
 
To help you understand the math, here is a hypothetical vesting chart showing how much the employeed would get if they left the company.
 
In this example the team member's salary is $50,000 USD and they max out their match every year. 50,000 * 6% = 3000. 3000 * 50% = 1500 match.
 
Year 1: Put in 3K, GitLab matches 1500. Leave the company get 3K (none vested)
Year 2: Put in 3K, GitLab matches 1500. Leave the company get 6K own money + $750 USD vested contribution.   (3K * 0.25)
Year 3: Put in 3K, GitLab matches 1500. Leave the company get 9K own money + $2250 USD vested contribution. (4500 * 0.50)
Year 4: Put in 3K, GitLab matches 1500. Leave the company get 12K own money + $4500 USD vested contribution. (6000 * 0.75)
Year 5: Put in 3K, GitLab matches 1500. Leave the company get 15K own money + $7500 USD (fully vested)

The above example assumed no earnings on the contributions to the team member's 401(k). If the team member has any gains on their own contributions, this would be considered their own money and can be withdrawn when leaving the company. If the team member has any gains on the employer contribution, this would be subject to the vesting schedule. For example, if my employer contribution earns $50 and I leave after year two, I would be 50% vested and be able to leave with $25 of the gains from the employer contribution.

**Administration of the 401(k) Match:**
* The employer will use the calculation on each check date effective as of January 1, 2019.
* The team member must have a contribution for a check date to be eligible for the employer match.
* Employer matching will be released into participant accounts three business days after the check date.
* For team members who defer more than 6% on each check date, Payroll will conduct a true up quarterly.
 
### 401(k) Rollover
 
If you leave GitLab and would like to rollover your 401(k) account, contact Betterment directly to get more information about this process. If you are needed to locate your account number, you can find it by clicking Settings and then Accounts. You can reach Betterment, by Chat in the app, by phone at 855-906-5281, and by email at `support@betterment.com`. They also have a Rollovers section on their site going into detail.
 
### 401(k) Committee
 
The 401(k) Committee will meet quarterly with Betterment to review how the plan is doing as well as updates from the Betterment investment team.
 
**Committee Members:**
Chair: Principal Accounting Officer (PAO)
**Other Members:**
* Principal Accounting Officer
* Chief Legal Officer
* Manager, Total Rewards
* Senior Director, People Success
* Senior Manager, Payroll and Payments
 
**Gitlab's 401(k) Committee Responsibilities:**
* Maintain and enforce the plan document and features
* Comply with all reporting, testing and disclosure requirements
* Timely data & deposit transmission
* Evaluating plan services and fees
* Review and approval of prepared forms and distributions
* Employee engagement and eligibility
* Adding new eligible employees to the plan
 
**Betterment's Responsibilities (co-fiduciary):**
* Investment selection and monitoring
* Audit support
* Employee education
* Administrative support on distributions, loans and more
* Employee and Plan Sponsor customer support via email and phone
* Statement and tax form generation
 
## Optional Plans Available at Team Member Expense
 
### WEX (Discovery Benefits) Health Savings Accounts and Flexible Spending Accounts
 
If you are enrolled in a Health Savings Account (HSA), Flexible Spending Account (FSA), Dependent Care Flexible Spending Account (DCFSA) or commuter benefits, the funds are held through WEX (Discovery Benefits). After your benefit enrollment effective start date, [create an account with WEX (Discovery)](https://benefitslogin.discoverybenefits.com/Login.aspx?ReturnUrl=%2f) to manage your funds. You will only receive one debit card upon enrollment. To obtain a second card (for a dependent, etc.) you will need to login to your account on WEX (Discovery) or call and they will send one to your home of record.

#### Health Savings Account (HSA)

GitLab contributes $100 per month for those enrolled in the **team member only** tier of coverage. There is no contribution for other tiers of coverage.

If you would like to transfer your HSA from a previous account, please contact WEX (Discovery) and request a HSA Transfer funds form. On the form you will put your old HSA provider’s account number and any other required personal information. You will then submit the form to WEX (Discovery), and they will get in contact with your old HSA provider and process the transfer of funds. You can reach WEX (Discovery) at 866.451.3399 or `customerservice@discoverybenefits.com`. 

If you would like to adjust your HSA contributions please log into [PlanSource](https://benefits.plansource.com/. 

HSAs roll over completely year-to-year and are 'owned' by the team member. If you leave GitLab and would like to keep your HSA account, GitLab will no longer be responsible for the administration fee. The account holder will become responsible for the $2.50 per month admin fee.
 
Domestic Partner Reimbursements: If the team member is not legally married to their domestic partner, the domestic partner's expenses are not eligible for disbursement from the HSA. However, if the domestic partner is covered under the family HDHP through the team member, the domestic partner can open their own HSA and contribute up to the full family contribution maximum. The team member may also contribute up to the full family contribution maximum to their own HSA.

#### Flexible Spending Account (FSA) Plans

FSAs help you pay for eligible out-of-pocket health care and dependent day care expenses on a pretax basis. You determine your projected expenses for the Plan Year and then elect to set aside a portion of each paycheck into your FSA.

FSAs have a $550 rollover each calendar year. This is subject to change as dictated by the IRS.

FSAs are employer-owned accounts. If you leave GitLab, your FSA account will be terminated on your date of termination unless you continue this through COBRA. You are able to use your full FSA amounts up to and on your last day, but not afterwards unless you enroll into COBRA. WEX (Discovery) asks that all claims be submitted up to 90 days after termination date. You can enroll into COBRA FSA if you have spent less out of your account than you have contributed to it. If you decide to enroll, you will be required to continue making your monthly contributions on a post-tax status.  For additional information, please reach out to WEX (Discovery) at 866.451.3399 or `customerservice@discoverybenefits.com`.
 
FSAs (and Dependent Care FSAs) are subject to annual testing by the Internal Revenue Code guidelines to ensure the pre-tax benefits do not disproportionately benefit highly compensated employees. At the end of each plan year, GitLab will work with WTW to conduct the nondiscrimination testing. If a test returns that highly compensated employees benefit, you may not be able to pre-tax the full amount of your election. This can vary each year and is based upon the dollar amount of non-highly compensated employees’ elections.
 
##### FSA Period to Submit Claims
 
Up to 90 days after the plan year has concluded (also known as the runout period).
 
#### Commuter Benefits
 
GitLab offers [commuter benefits](https://drive.google.com/file/d/0B4eFM43gu7VPek1Ia0ZqYjhuT25zYjdYTUpiS1NFSXFXc0Vn/view?usp=sharing) which are administered through WEX (Discovery Benefits). The contribution limits from the IRS for 2020 are $270/month for parking and $270/month for transit. These contributions rollover month to month.
 
##### Commuter Benefits Period to Submit Claims
 
For active employees: Up to 180 days after plan year has concluded (also known as the runout period).
 
For terminated employees: Up to 90 days after the termination date.
 
### Cigna Supplemental Life Insurance/AD&D
 
Team members who wish to elect additional life insurance above what is provided by GitLab through the Basic Life Insurance or elect life insurance for their dependents, can elect [Voluntary Life Insurance](https://drive.google.com/file/d/1yY1tFJTuj1wCMlorUvnfcgWwZUuP279r/view?usp=sharing) through Cigna.
 
  * $10,000 Increments up to the lesser of 6x annual salary or $750,000 for team members
  * $5,000 Increments up to the lesser of $250,000 or 100% of team member election for spouses and domestic partners
  * $10,000 of coverage available for children
 
##### Evidence of Insurability
 
If you elect supplemental life insurance greater than the guaranteed issue of voluntary life insurance for yourself or dependent, you or your dependent may be required to complete an evidence of insurability (EOI). If required, you will be prompted and provided with the form during the enrollment process and can also access it [here](https://drive.google.com/file/d/1Q1AjhGGia1QT2U_OC7TnMUtTk50ZuLBN/view?usp=sharing).
 
Please complete this form to the best of your ability, but if you're unsure for any field, please leave it blank. No information needs to be filled out for the ID # field and for security, you may also leave the Social Security Number field blank.
 
Once complete, please send the form to `total-rewards@ gitlab`. The Total Rewards team will then help fill in any missing information, if applicable, and will forward to the carrier for review. Total Rewards will confirm receipt of the EOI with the carrier, track its status, and the team or the carrier will reach out to the team member with any issues that need to be addressed in order for the EOI to be approved.
 
If you leave GitLab, all supplemental life insurance is not included in COBRA and will terminate on your last day at GitLab unless you choose to continue your or your spouse/children's life insurance directly with the carrier. Please contact the carrier (Cigna) for more information on premiums and setting this up.
 
## Team Member Discount Platforms
 
US team members have access to a discount platform offered through ADP. This platform provides discounts for national and local brands and services.
 
To access LifeMart through ADP:
1. Login to ADP using the following link: [(https://workforcenow.adp.com.)]
1. Click on the "MYSELF" tab in the navigation bar, hover your mouse over "Benefits" in the dropdown menu, and click "Employee Discounts - Life Mart".
1. Confirm the email you use to access ADP and click "View my discounts" to enter the website.
 
## GitLab Inc. United States Leave Policy
 
Based on the Family and Medical Leave Act, or [FMLA](https://www.dol.gov/agencies/whd/fmla), US team members are "eligible to take job-protected leave for specified family and medical reasons with continuation of group health insurance coverage under the same terms and conditions as if the team member had not taken leave." For more information on what defines an eligible team member and medical reason please see [Employee Rights Under the Family and Medical Leave Act](https://www.dol.gov/sites/dolgov/files/WHD/legacy/files/fmlaen.pdf).
 
### Apply For Parental Leave in the US
 
1. Notify the Total Rewards team of intention to take parental leave at least 30 days in advance, or as soon as reasonable by entering the dates in PTO Roots via Slack.
2. For a birthing parent (maternity leave), the team member will fill out the [Leave Request Form](https://drive.google.com/file/d/1guydUTEc0vBFMaa_IsSktZ5hXAbOXdvD/view?usp=sharing) and [Assignment of Benefits](https://drive.google.com/file/d/1pGqQsuzk3aEdG4srj78fXWrsRfb7jqB9/view?usp=sharing) and email the completed forms to `total-rewards@gitlab.com`.
     - The Assignment of benefits form states that Cigna will pay GitLab the funds and GitLab will disperse those funds to the team member. The intention of this form is to keep payroll consistent for the team member via GitLab payroll vs needing to offset disability payments from Cigna. GitLab may need to offset any state related payments if applicable.
     - Please see the Short-Term Disability claims process for more information on how the claim will be [processed with Cigna](/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/#group-long-term-and-short-term-disability-insurance) as well as specifics on how to fill out the Leave Request Form.
1. The Total Rewards team will send the team member an email with how payments will be processed and advise any differences in pay.
1. The Total Rewards team will confirm [payroll details](#payroll-processing-during-parental-leave) with the Payroll team via the Payroll Changes google sheet.
1. The team member will notify the Total Rewards team on their first day back to work.
1. The Total Rewards team will get a notification once the claim is closed.
1. TODO Outline process to return the team member to work
 
### Payroll Processing During Parental Leave
 
**Paternity Leave**
Paternity Leave is not covered under Short-Term Disability, so if the team member is eligible for 100% of pay, payroll would enter 100% of the pay period hours under "Leave with Pay." Paternity Leave is also offset for any state eligible payments.
 
**Maternity Leave**
For maternity leave, GitLab will verify 100% of the wages are paid for eligible team members through payroll, Short-term Disability (STD), and state leave pay (where applicable).
 
Parental leave is inclusive of public holidays that occur during within the start and end date of parental leave. GitLab will recieve the Short-Term Disability funds directly from Cigna and keep the GitLab team member paid at 100% of wages via payroll. Total Rewards will notify payroll of the "leave with pay" status using the start and end date of parental leave. Total Rewards will also notify payroll of any state related payments which may need to be offset.
 
## State-Specific Allowed Leaves
 
Each US state varies when it comes to types of leave employers are required to allow employees to take. GitLab's [Paid Time Off policy](/handbook/paid-time-off/) policy of "no ask, must tell" takes precendence but we still want our US team members to be aware of their specific types of leave in their state.
 
| Leave Type                              | State or City/Region |
|-----------------------------------------|-------|
| Bereavement Leave                       | IL, OR, WA (Tacoma)  |
| Blood/Bone Marrow/Organ Donation Leave  | AR, CA, CT, HI, IL, LA, ME, MD, MN, NE, NJ, NY, OR, SC, WI |
| Court Attendance/Witness Leave          | CA, CT, DC, FL, GA, HI, IL, IN, IA, KY, MD, MA, MN, MO, NV, ND, OH, OR, PA, RI, TX, UT, VT, VI, WI, WY |
| Crime Victims' Leave                    | AL, AK, AZ, AR, CA, CO, CT, DE, MD, MA, MI, MN, MS, MO, MT, NH, NY, OH, OR, PA, RI, SC, VT, VA, WY |
| Domestic/Sexual Violence Victims' Leave | AZ, AR, CA, CO, CT, DC, FL, HI, IL, IA, KS, ME, MD, MA, MI, MN, NV, NJ, NM, NY, NC, OR, PA, RI, TX, VT, VA, WA |
| Drinking and Driving Class Leave        | IA      |
| Drug/alcohol Rehabilitation Leave       | CA      |
| Election Officials' Leave               | AL, CA, DE, IL, KY, MN, NE, NC, OH, VA, WI  |
| Emancipation Day Leave                  | DC      |
| Emergency Evacuation Leave              | TX      |
| Legislative/Political Leave             | CT, IA, ME, MN, MT, NV, OK, OR, SD, TX, VT, WV      |
| Literacy Leave                          | CA      |
| Mobile Support Unit Leave               | IN      |
| Paid Family Leave                       | CA, CO, CT, DC, MA, NJ, NY, OR, RI, WA       |
| Pregnancy Disability Leave              | CA, CT, HI, IA, LA, MN, MT, NH, OR, WA      |
| Public Health Emergency (Quarantine/Isolation) Leave | AZ, CA (San Diego), CO, DE, IL (Cook Co), MA, MD (Montgomery Co), MI, MN, NJ, NY (Westchester Co & NYC), OR, PA (Pittsburgh), RI, SC, VT, WA  |
| School Activities/Visitation Leave      | CA, DC, IL, LA, MA, MN, NV, NJ, NC, OR, RI, TN, VT   |
| Student Leave                           | DC      |
| Volunteer Emergency Responder Leave     | AL, CA, CO, CT, DE, IL, IN, IA, KS, KY, LA, ME, MD, MA, MO, NE, NV, NH, NJ, NM, NY, NC, ND, OH, OR, PA, RI, SC, TN, UT, WA, WV, WI |
| Voting Leave                            | AL, AK, AZ, AR, CA, CO, DC, GA, IL, IA, KS, KY, MD, MA, MN, MO, NE, NV, NM, NY, ND, OH, OK, SD, TN, TX, UT, VT, WV, WI, WY   |
 
## COBRA
 
If you are enrolled in medical, dental, and/or vision when you terminate from GitLab (either voluntarily or involuntarily), you may be eligible to continue your coverage through [COBRA](https://www.dol.gov/sites/dolgov/files/ebsa/about-ebsa/our-activities/resource-center/faqs/cobra-continuation-health-coverage-consumer.pdf).
 
### Timeline
1. Typically terminations are updated in BambooHR on the date of the termination and once updated, will then be updated in PlanSource by the end of the next business day.
1. Once the termination has been added to PlanSource, the information will then be sent over to [WEX (Discovery Benefits)](https://cobra.discoverybenefits.com/), our COBRA administrator. Government guidelines give 30 days for WEX (Discovery) to be notified of the COBRA eligibility, but typically this will take about 1-2 weeks.
1. Once notified, WEX (Discovery) has 14 days to generate and send the COBRA enrollment packet. Allow normal mailing timelines (5-10 business days) to receive the packet once sent. You may also contact `total-rewards@ gitlab` at least a week after your termination date and we can send an electronic copy of your COBRA enrollment packet to you if it has been generated.
1. You will have 60 days from the time you receive the COBRA packet to enroll either through the mail or online. Instructions for how to enroll will be included in your COBRA packet. Coverage will be retro-effective to the date coverage was lost.
1. From the day you enroll, you have 45 days to bring your payments to current.
1. You may remain on COBRA for up to 18 months. Please see the COBRA enrollment packet for information on extending COBRA an additional 18 months, if applicable. The state you reside in may allow for additional time on COBRA, but may be more expensive and include only Medical. Please consult the laws for your state for more information.
 
If you are currently employed and have any general COBRA questions, feel free to contact the Compensation & Benefits team. If you have terminated already or have specific questions on the administration of COBRA, feel free to contact WEX (Discovery) Benefits directly: (866) 451-3399.
 
### Costs per Month
 
**Medical**

2021
| Tier                           | Cigna EPO   | Cigna HDHP HSA  |  Cigna PPO   | Kaiser HMO NorCal | Kaiser HMO SoCal | Kaiser HMO CO | Kaiser HMO HI |
|--------------------------------|:---------:|:-------------:|:----------:|:-----------------:|:----------------:|:--------------:|:------------:|
| Team Member Only               | $562.21   |   $423.75     | $549.39   |     $532.72       |     $532.72      |    $734.04     |   $495.41    |
| Team Member + Domestic Partner | $1,225.69   |   $917.57   | $1,197.64  |     $1,230.19     |     $1,230.19    |    $1,614.88   |   $990.83    |
| Team Member + Spouse           | $1,225.69   |   $917.57   | $1,197.64  |     $1,230.19     |     $1,230.19   |    $1,614.88   |   $990.83    |           
| Team Member + Child(ren)       | $1,063.98   |   $797.22     | $1,039.70    |     $1,086.73     |     $1,086.73   |    $1,468.08   |   $891.75    |
| Family                         | $1,699.62 |   $1,270.30   | $1,660.70  |     $1,613.74     |     $1,613.74    |    $2,202.12   |   $1,486.24  |
 
 2022
| Tier                           | Cigna EPO   | Cigna HDPHP HSA  |  Cigna PPO   | Kaiser HMO NorCal | Kaiser HMO SoCal | Kaiser HMO CO | Kaiser HMO HI |
|--------------------------------|:---------:|:-------------:|:----------:|:-----------------:|:----------------:|:--------------:|:------------:|
| Team Member Only               | $617.15   |   $461.04     | $602.85   |     $545.89       |     $545.89      |    $829.45     |   $523.05    |
| Team Member + Domestic Partner | $1,345.49 |   $1,003.65   | $1.314.21  |     $1,260.63     |     $1,260.63    |    $1,824.79   |   $1,046.11    |
| Team Member + Spouse           | $1,345.49   |   $1,003.65   | $1,314.21  |     $1,260.63     |     $1,260.63   |    $1,824.79   |   $1,046.11    |           
| Team Member + Child(ren)       | $1,167.99   |   $871.43     | $1,134.03    |     $1,113.62     |     $1,113.62   |    $1,658.90   |   $941.50    |
| Family                         | $1,865.77 |   $1,391.22   | $1,822.35  |     $1,653.67     |     $1,653.67    |    $2,488.36   |   $1,569.16  |


**Dental**
 
 2021
| Tier                           | Cigna DPPO |
|--------------------------------|:----------:|
| Team Member Only               |   $39.77   |
| Team Member + Domestic Partner |   $79.03   |
| Team Member + Spouse           |   $79.03   |
| Team Member + Child(ren)       |   $91.41   |
| Family                         |   $140.24  |

2022
| Tier                           | Cigna DPPO |
|--------------------------------|:----------:|
| Team Member Only               |   $41.13   |
| Team Member + Domestic Partner |   $81.74   |
| Team Member + Spouse           |   $81.74   |
| Team Member + Child(ren)       |   $94.55   |
| Family                         |   $145.05  |
 
**Vision**
 
| Tier                           | Cigna VPPO |
|--------------------------------|:----------:|
| Team Member Only               |   $7.66    |
| Team Member + Domestic Partner |   $15.35   |
| Team Member + Spouse           |   $15.35   |         
| Team Member + Child(ren)       |   $12.99   |
| Family                         |   $21.43   |
