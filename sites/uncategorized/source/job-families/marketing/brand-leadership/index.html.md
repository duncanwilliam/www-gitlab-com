---
layout: job_family_page
title: "Brand Leadership"
---

## Director, Brand 

This role is a leader on the corporate marketing team who will provide vision for a team of creatives that are inspired to take GitLab to the next level. This role will set the GitLab brand vision and work to execute the strategy across various mediums. 

The Director, Brand reports to the VP, Corporate Marketing and oversees the design, brand growth and corporate events teams.

#### Director, Brand Job Grade

The Director, Brand is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Director, Brand Responsibilities

* Develop and oversee brand strategy to increase awareness of GitLab the brand as well as the product across the globe. 
* Bring the GitLab brand to life through one-of-a-kind brand campaigns and creative brand concepts that target our business and developer audiences. 
* Recruit top talent to build a world class brand and creative team to take GitLab to the next level as a company. 
* Work with the team to provide vision and strategy around brand campaigns to execute them in a results and metrics driven fashion.
* Manage joint marketing opportunities with customers and partners to drive GitLab’s brand narrative. 
* Bring on and manage creative agencies around the world; ensuring activities are aligned to the strategy.
* Oversee the creative, brand growth and corporate events team to align with overall brand objectives and messaging. 
* Oversee resources to ensure key activities are activated with the right budget at the right time, in-line with the GitLab brand. 
* Ability to think outside of the box and challenge the team to push the boundaries. 

#### Director, Brand Requirements

* 15+ years of experience working as a brand leader in B2B technology space. 
* Passion for creativity, innovation and making complex topics and systems accessible and understandable.
* A data-driven mindset with the ability to convert quantitative & qualitative research into actionable insights.
* Experience in coaching, developing and mentoring employees.
* Excellent program management skill set able to identify work streams, plan rollouts and execute.
* Background in building brands inclusive of audiences that span wide functions and geographies.
* Experience building and scaling processes, teams, budgets, tools, and programs.
* Ability to work cross-functionally with product, creatives, product marketers, content marketers, and campaign managers of all levels.

## Performance Indicators

* Sets brand goals and objectives for the organization. 
* Increases overall brand awareness through various channels and campaigns. 
* Builds and recruits a world-class brand team.

## Career Ladder

* The next step in the Brand Leadership job family is not yet defined at GitLab.

## Hiring process

Candidates for these positions can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our team page.

* Qualified candidates will be invited to schedule a 30 minute screening call with one of our Global Recruiters
* Next, candidates will be invited to schedule a first interview with the Hiring Manager
* Next, candidates will be invited to interview with 2-5 team members
* There may be a final executive interview

Additional details about our process can be found on our [hiring page](/handbook/hiring/).
