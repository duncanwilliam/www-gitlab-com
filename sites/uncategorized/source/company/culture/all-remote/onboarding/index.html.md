---
layout: handbook-page-toc
title: "The guide to remote onboarding"
canonical_path: "/company/culture/all-remote/onboarding/"
description: How to handle onboarding in a remote company
twitter_image: "/images/opengraph/all-remote.jpg"
twitter_image_alt: "GitLab remote team graphic"
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

## On this page
{:.no_toc}

- TOC
{:toc}
## Introduction

![GitLab customer path](/images/all-remote/gitlab-customer-path.jpg){: .medium.center}

On this page, we're detailing how to successfully onboard new hires in a remote environment.

## How to onboard new hires remotely

Onboarding remotely should focus on [three key dimensions](https://hbr.org/2018/12/to-retain-new-hires-spend-more-time-onboarding-them): the organizational, the technical, and the social. By using this integrated approach, top companies enable their team members to stay and thrive in their roles. We'll show how you can focus on these three key dimensions of onboarding through an all-remote onboarding process.

### Organizational onboarding 

A large part of onboarding is providing answers to logistical questions:

1.  How do I sign up for benefits?
1. What tools should I use?
1.  Where do I go to find answers?
1. Who can help me with ____?

Traditional in-person companies usually rely on trainers or more hands-on approaches to help new hires navigate their surroundings. All-remote companies have to be more efficient and make information easily accessible, so documentation will be essential for a smooth onboarding process. At GitLab, we provide a detailed [handbook](/handbook/) that is always evolving. 

The GitLab team handbook is the central repository for how we run the company. It consists of over [2,000 webpages of text](/handbook/about/#count-handbook-pages), all searchable of course. Our handbook serves as a [single source of truth](https://docs.gitlab.com/ee/development/documentation/styleguide.html#documentation-is-the-single-source-of-truth-ssot) that all team members can reference and depend on for answers about GitLab. 

For onboarding, we're able to direct new hires to a huge repository of information and also teaching them to be self-sufficient and proactive when looking for answers. Because we've implemented a [handbook-first approach](/company/culture/all-remote/handbook-first-documentation/#make-handbook-first-documentation-a-value), the GitLab handbook is always changing and growing as we learn new things.

In an all-remote setting where team members are possibly working from a variety of timezones, mastering [asynchronous workflows](/company/culture/all-remote/asynchronous/#how-to-implement-asynchronous-workflows) is vital to avoiding dysfunction. Onboarding through documentation is more efficient because it's scalable, repeatable, and instills the basics of asynchronous work.

**Additional resources:**

1. [GitLab onboarding](/handbook/people-group/general-onboarding/)
1. [Guide for starting a remote job](/company/culture/all-remote/getting-started/)
1. [Adopting a self-service and self-learning mentality](/company/culture/all-remote/self-service/)

### Technical onboarding 

While tools are an important part of any role, new hires need to feel empowered to use them. Organizations can help build technical confidence by setting up early wins with action items the new hire can complete as they move through their training. Organizational onboarding provides the access to information through handbooks/documentation, and technical onboarding is about using that knowledge to work through the tools.

At GitLab, we practice [dogfooding](/handbook/values/#dogfooding). Our entire company uses GitLab to collaborate on the handbook and we also create issues and merge requests in our product. For all new GitLab hires, we have created an [onboarding issue template](https://gitlab.com/gitlab-com/people-group/employment-templates-2/-/blob/master/.gitlab/issue_templates/onboarding.md) that has tasks to complete each day. We believe in using the tools we create. This allows new hires to become familiar with GitLab in a way that feels meaningful (e.g. in learning GitLab, they are also accomplishing necessary onboarding tasks). This also provides a continual set of new users to test GitLab with fresh eyes. These individuals are ideally positioned to point out missing features or areas for improvement as we iterate on the product.

For technical onboarding, give new hires access to the tools they'll be using in their roles and, most importantly, encourage them to use the tools as early as possible. Using tools, even for very small tasks, builds confidence and helps new employees to feel productive and empowered.

**Additional resources:**

1. [All-remote management](/company/culture/all-remote/management/)
1. [Tools that enable remote teams](/company/culture/all-remote/resources/#tools-that-enable-remote-teams)
1. [3 things I learned in my first month at GitLab](/blog/2016/11/02/three-things-i-learned-in-my-first-month-at-gitlab/)


### Social onboarding 

Starting a new job can be overwhelming. If a new hire is used to working in a traditional office, adjusting to the remote work lifestyle might be a challenge. Having socialization as part of the onboarding process can help team members feel more connected to their new teams, even though they don't share an office.

In an all-remote company, it's important to encourage [informal communication](https://about.gitlab.com/company/culture/all-remote/informal-communication/) so that team members can build relationships. This can be incorporated into the onboarding process in a couple of key ways:

1. **Assign an onboarding buddy.** This individual can be a friendly point of contact for a new team member and also introduce them to others. Onboarding buddies often set the expectation for how to build relationships with other team members, so new hires and onboarding buddies should communicate in a variety of ways, such as video calls, check-ins on Slack, and coffee chats. At GitLab, we take this kind of role seriously and have an entire handbook page dedicated to [onboarding buddies](/handbook/people-group/general-onboarding/onboarding-buddies/). 

1. **Formally design informal communication.** In an all-remote environment, informal communication should be formally addressed. Leaders should organize informal communication and provide structured opportunities for new hires to get to know their coworkers. We incorporate socializing tasks into our onboarding template such as [scheduling coffee chats](/company/culture/all-remote/informal-communication/#scheduling-a-coffee-chat), introducing yourself in the `#new_team_members` slack channel, or participating in a video call.

**Additional resources:**

1. [Building and reinforcing a sustainable culture](/company/culture/all-remote/building-culture/)
1. [Considerations for in-person interactions in a remote company](/company/culture/all-remote/in-person/)
1. [Social slack channel ideas](/handbook/communication/chat/#social-groups)

## The importance of onboarding

![GitLab collaboration lightbulb](/images/all-remote/gitlab-collaboration-illustration.jpg){: .medium.center}

Onboarding is the official process of integrating new team members into an organization. The difference between onboarding and orientation is that orientation is typically a singular event, whereas onboarding is a continuous process during an team member's first year. Having an efficient onboarding process can have long-term benefits:

1. Better team member retention
1. More productive, more quickly
1. Reduces anxiety
1. Sets expectations

Onboarding is an investment in the long term success of new hires and in the company as well. Studies show that [87% of team members are less likely to leave a company](http://www.totalteambuilding.com.au/the-positive-impact-of-team-building/) when they feel engaged. Companies that invest in onboarding report [54% more productivity](https://www.myshortlister.com/insights/employee-onboarding-statistics) from new hires.

When [hiring in an all-remote organization](/company/culture/all-remote/hiring/), a cohesive onboarding process is especially important because not only do you have to give new hires the information they need to do their jobs, you have to empower them to think remotely as well. Remote onboarding relies heavily on [documentation](/company/culture/all-remote/management/#scaling-by-documenting), anticipating the needs of new team members, and a dedication to continuously improving the process based on feedback.

## High-touch and low-touch flexibility

The beauty of an all-remote setting is that onboarding can be as high-touch or low-touch as a new hire wishes. Those who prefer visual learning can engage in a series of video calls and screenshare sessions to walk through each element of their onboarding issue. Those who prefer more self-learning can benefit from thorough documentation and readily-available resources for self-guided learning.

That flexibility is unique to the all-remote environment, as those who prefer a self-guided experience are typically forced into a very social onboarding at colocated companies.

## Empowerment from day one

![GitLab values](/images/all-remote/gitlab-values-tanukis.jpg){: .medium.center}

The onboarding process should be empowering. At GitLab, if a new hire gets stuck during onboarding, they are encouraged to update the handbook, record a video to help others who may encounter the same obstacle, and contribute to learning and development from day one.

Learn more about our approach to [All-Remote Learning and Development](/company/culture/all-remote/learning-and-development/#how-do-you-onboard-new-team-members).

----

Return to the main [all-remote page](/company/culture/all-remote/).
